﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Space.Control {
    [RequireComponent(typeof(BasicMovement))]
    public class BorderCollision : MonoBehaviour
    {
        [SerializeField] float borderBoostDuration = 0.3f;

        BasicMovement basicMovement;

        void Start()
        {
            basicMovement = GetComponent<BasicMovement>();
        }

        void OnCollisionEnter(Collision other)
        {
            ContactPoint contact = other.contacts[0];
            if (contact.otherCollider.tag == "VerticalBorder")
            {
                HandleVerticalCollision();
            }
            else if (contact.otherCollider.tag == "HorizontalBorder")
            {
                HandleHorizontalCollision();
            }

            basicMovement.Boost(borderBoostDuration);
        }

        void HandleVerticalCollision()
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 180f - basicMovement.GetAngle());

            Vector3 direction = basicMovement.Direction;
            direction.y = - direction.y;
            basicMovement.Direction = direction;
        }

        void HandleHorizontalCollision()
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 360f - basicMovement.GetAngle());

            Vector3 direction = basicMovement.Direction;
            direction.x = -direction.x;
            basicMovement.Direction = direction;
        }
    }
}
