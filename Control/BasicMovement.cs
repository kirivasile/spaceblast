﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Space.Control {
    public class BasicMovement : MonoBehaviour
    {
        [SerializeField] Vector3 direction = Vector3.up;
        [SerializeField] Vector3 startPosition = Vector3.zero;
        [SerializeField] float normalSpeed = 1f;

        float speed;
        float timer;
        float boostEndTime;

        bool isBoosting = false;
        bool isActive = false;

        Fire fire;

        // Start is called before the first frame update
        void Start()
        {
            fire = GetComponent<Fire>();
        }

        public void Reset() {
            transform.position = startPosition;

            timer = 0f;
            boostEndTime = -1f;
            speed = normalSpeed;
            transform.rotation = Quaternion.Euler(0f, 0f, GetAngle());

            direction = Vector3.Normalize(direction);

            if (fire != null) {
                fire.NormalFire();
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (isActive) {
                HandleMovement();
                HandleBoosting();

                timer += Time.deltaTime;
            }
        }

        void HandleMovement() {
            if (speed > 0f)
            {
                Vector3 delta = direction * speed * Time.deltaTime;
                delta.z = 0f;
                transform.position += delta;
            }
        }

        void HandleBoosting() {
            if (boostEndTime > timer)
            {
                isBoosting = true;
                speed = 2 * normalSpeed;

                if (fire != null) {
                    fire.DoubleFire();
                }
            }
            else if (isBoosting)
            {
                isBoosting = false;
                speed = normalSpeed;

                if (fire != null) {
                    fire.NormalFire();
                }
            }
        }

        public float GetAngle() {
            float angle = -Mathf.Atan(direction.x / direction.y) * Mathf.Rad2Deg;
            if (direction.y < 0) {
                angle -= 180f;
            }
            return angle;
        }

        public void Boost(float boostDuration) {
            boostEndTime = timer + boostDuration;
        }

        public void Enable() {
            isActive = true;
        }

        public Vector3 Direction {
            get {
                return direction;
            }
            set {
                direction = value;
            }
        }

    }
}
