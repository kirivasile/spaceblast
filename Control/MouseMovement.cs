﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Space.Control {
    [RequireComponent(typeof(BasicMovement))]
    public class MouseMovement : MonoBehaviour
    {
        BasicMovement basicMovement;

        [SerializeField] float rotationBoostDuration = 0.5f;
        [SerializeField] float rotationSpeed = 1f;
        [SerializeField] float rotationMinDistance = 0.5f;
        [SerializeField] GameObject gamePlane;

        // Start is called before the first frame update
        void Start()
        {
            basicMovement = GetComponent<BasicMovement>();
        }

        // Update is called once per frame
        void Update()
        {
            HandleMouseClick();
        }

        void HandleMouseClick()
        {

            if (Input.GetMouseButton(0))
            {
                Vector3 mousePosition;
                bool result = GetMousePosition(out mousePosition);
                if (!result)
                {
                    return;
                }
                mousePosition.z = 0f;

                Vector3 directionToMouse = mousePosition - transform.position;
                directionToMouse.z = 0f;

                if (directionToMouse.magnitude < rotationMinDistance)
                {
                    return;
                }

                basicMovement.Boost(rotationBoostDuration);

                float angleDelta = rotationSpeed * Time.deltaTime;
                Vector3 newDirection = Vector3.RotateTowards(basicMovement.Direction, directionToMouse, angleDelta, 0f);
                newDirection.z = 0f;
                basicMovement.Direction = Vector3.Normalize(newDirection);

                transform.rotation = Quaternion.Euler(0f, 0f, basicMovement.GetAngle());
            }   

        }

        bool GetMousePosition(out Vector3 position)
        {
            Vector3 mousePosition = Input.mousePosition;
            RaycastHit[] hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(mousePosition));

            if (!CheckMouseInsideScreen(mousePosition))
            {
                position = Vector3.zero;
                return false;
            }

            foreach (RaycastHit hit in hits)
            {
                if (hit.transform.gameObject == gamePlane)
                {
                    position = hit.point;
                    return true;
                }
            }

            position = Vector3.zero;
            return false;
        }

        bool CheckMouseInsideScreen(Vector3 mousePosition)
        {
    #if UNITY_EDITOR
            if (
                mousePosition.x <= 0 ||
                mousePosition.y <= 0 ||
                mousePosition.x >= Handles.GetMainGameViewSize().x - 1 ||
                mousePosition.y >= Handles.GetMainGameViewSize().y - 1
            )
            {
                return false;
            }
    #else
            if (
                mousePosition.x <= 0 || 
                mousePosition.y <= 0 || 
                mousePosition.x >= Screen.width - 1 || 
                mousePosition.y >= Screen.height - 1
            ) { 
                return false;
            }
    #endif
            return true;
        }
    }
}
