using UnityEngine;

namespace Space.Control {
    public class Fire : MonoBehaviour {
        [SerializeField] Transform fire;

        public void DoubleFire() {
            fire.localPosition = new Vector3(0f, -0.97f, 0f);
            fire.localScale = new Vector3(0.5f, 0.5f, 1f);
        }

        public void NormalFire() {
            fire.localPosition = new Vector3(0f, -0.78f, 0f);
            fire.localScale = new Vector3(0.3f, 0.3f, 1f);
        }
    }
}