﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Border : MonoBehaviour
{
    [SerializeField] Transform leftBorder;
    [SerializeField] Transform rightBorder;
    [SerializeField] Transform topBorder;
    [SerializeField] Transform bottomBorder;

    float borderConst = 0.62f;

    // Start is called before the first frame update
    void Start()
    {
        AlignLeft();
        AlignRight();
        AlignTop();
        AlignBottom();
    }

    void AlignLeft() {
        Vector3 position = Camera.main.ViewportToWorldPoint(new Vector3(0f, 0.5f, 0f));
        position.x -= leftBorder.localScale.x * borderConst;
        position.z = 0f;
        leftBorder.position = position;
    }

    void AlignRight() {
        Vector3 position = Camera.main.ViewportToWorldPoint(new Vector3(1f, 0.5f, 0f));
        position.x += rightBorder.localScale.x * borderConst;
        position.z = 0f;
        rightBorder.position = position;
    }

    void AlignTop() {
        Vector3 position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1f, 0f));
        position.y += topBorder.localScale.y * borderConst;
        position.z = 0f;
        topBorder.position = position;
    }

    void AlignBottom() {
        Vector3 position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0f, 0f));
        position.y -= bottomBorder.localScale.y * borderConst;
        position.z = 0f;
        bottomBorder.position = position;
    }
}
