﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Space.Core {
    public class LevelEventSystem : MonoBehaviour
    {
        public static LevelEventSystem instance;

        private void Awake() {
            instance = this;
        }

        public event Action onGameOver;

        public void GameOver() {
            if (onGameOver != null) {
                onGameOver();
            }
        }

        public event Action<int> onExplosionDeath;

        public void ExplosionDeath(int id) {
            if (onExplosionDeath != null) {
                onExplosionDeath(id);
            }
        }
    }
}
