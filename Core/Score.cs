﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Space.Core {
    public class Score : MonoBehaviour
    {
        Text text;

        float timer = 0f;
        bool isActive;

        // Start is called before the first frame update
        void Start()
        {
            isActive = false;
            text = GetComponent<Text>();
        }

        // Update is called once per frame
        void Update()
        {
            if (isActive) {
                timer += Time.deltaTime;

                int seconds = (int)(timer);
                text.text = string.Format("TIME {0}", seconds);
            }
        }

        public void Enable() {
            isActive = true;
        }

        public void Disable() {
            isActive = false;
        }

        public void Restart() {
            timer = 0f;
            isActive = true;
        }
    }
}
