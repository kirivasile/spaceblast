﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Space.Level {
    public class ExplosionPool : MonoBehaviour
    {
        [SerializeField] int startSize;
        [SerializeField] Explosion prefab;

        List<Explosion> explosions;

        private void Start() {
            explosions = new List<Explosion>();

            for (int i = 0; i < startSize; ++i) {
                Explosion explosion = Instantiate<Explosion>(prefab, Vector3.zero, Quaternion.identity, transform);
                explosions.Add(explosion);
            }
        }

        public Explosion Get(Vector3 position) {
            for (int i = 0; i < explosions.Count; ++i) {
                if (!explosions[i].IsActive()) {
                    explosions[i].transform.position = position;
                    explosions[i].Enable();
                    return explosions[i];
                }
            }

            Explosion explosion = Instantiate<Explosion>(prefab, position, Quaternion.identity, transform);
            explosions.Add(explosion);
            explosion.Enable();
            return explosion;
        }

        public void DisableAll() {
            foreach (Explosion explosion in explosions) {
                explosion.Disable();
            }
        }
    }
}
