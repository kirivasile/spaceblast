﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Space.Control;
using Space.Core;

namespace Space.Level {
    public class LevelManager : MonoBehaviour
    {
        [SerializeField] GameObject player;
        [SerializeField] ExplosionPool explosionPool;

        [SerializeField] float difficultyIncreasePeriod = 5f;
        [SerializeField] float timeBetweenExplosions = 5f;
        [SerializeField] float difficultyMultiplier = 0.9f;
        [SerializeField] float minTimeBetweenExplosions = 0.5f;

        [SerializeField] float minX, maxX;
        [SerializeField] float minY, maxY;

        [SerializeField] GameObject greetingsPanel;
        [SerializeField] GameObject gameOverPanel;
        [SerializeField] Score scorePanel;

        float startTimeBetweenExplosions;
        float exlosionTimer = 0f;

        bool gameOver = false;
        bool gameStarted = false;

        private void Start() {
            startTimeBetweenExplosions = timeBetweenExplosions;
            LevelEventSystem.instance.onGameOver += GameOver;
        }

        private void Update() {
            if (gameStarted) {
                CreateExplosion();
                exlosionTimer += Time.deltaTime;
            }
        }

        void CreateExplosion() {
            if (!gameOver && exlosionTimer > timeBetweenExplosions) {
                exlosionTimer = 0f;

                Vector3 position = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 1f);

                explosionPool.Get(position);

                timeBetweenExplosions = Mathf.Max(timeBetweenExplosions * difficultyMultiplier, minTimeBetweenExplosions);
            }
        }

        public void GameOver() {
            if (!gameOver) {
                gameOver = true;

                explosionPool.DisableAll();
                
                player.SetActive(false);
                gameOverPanel.gameObject.SetActive(true);
                scorePanel.Disable();
            }
        }

        public void RestartGame() {
            player.GetComponent<BasicMovement>().Reset();

            timeBetweenExplosions = startTimeBetweenExplosions;
            exlosionTimer = 0f;

            gameOver = false;

            gameOverPanel.gameObject.SetActive(false);
            player.SetActive(true);

            scorePanel.Restart();
        }

        public void StartGame() {
            greetingsPanel.SetActive(false);
            gameStarted = true;
            scorePanel.Enable();

            player.GetComponent<BasicMovement>().Reset();
            player.GetComponent<BasicMovement>().Enable();
        }
    }
}