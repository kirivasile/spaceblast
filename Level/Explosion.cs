﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Space.Core;

namespace Space.Level {
    public class Explosion : MonoBehaviour
    {
        [SerializeField] float lifeDuration;
        [SerializeField] float safeLifeDuration;
        [SerializeField] float sizeSpeed;

        MeshRenderer meshRenderer;
        Animator explosionAnimation;
        SpriteRenderer explosionRenderer;

        float timer;
        float radius;

        bool isDangerous = false;
        bool isDeadByTimer = false;

        GameObject player;

        bool isActive = true;

        private void Start() {
            player = GameObject.FindGameObjectWithTag("Player");
            meshRenderer = GetComponent<MeshRenderer>();

            explosionAnimation = GetComponentInChildren<Animator>();
            explosionRenderer = GetComponentInChildren<SpriteRenderer>();
            Disable();  
        }

        private void Update() {
            if (isActive) {
                ChangeSize();
                CheckCollision();
                Life();

                timer += Time.deltaTime;
            }
        }

        private void ChangeSize()
        {
            radius += sizeSpeed * Time.deltaTime;

            transform.localScale = new Vector3(radius, radius, 0f);
        }

        private void Life() {
            if (!isDangerous && timer > safeLifeDuration) {
                meshRenderer.material.color = Color.red;
                isDangerous = true;
            }
            if (!isDeadByTimer && timer > lifeDuration) {
                isDeadByTimer = true;
                StartCoroutine(Explode());
            }
        }

        void CheckCollision() {
            if (isDeadByTimer) {
                return;
            }

            Vector3 playerPosition = player.transform.position;
            playerPosition.z = 0f;

            Vector3 explosionPosition = transform.position;
            explosionPosition.z = 0f;

            float distance = Vector3.Distance(playerPosition, explosionPosition) * 1.9f;

            if (isDangerous && distance < transform.localScale.x) {
                LevelEventSystem.instance.GameOver();
            }
        }

        public void Enable() {
            Reset();
            isActive = true;
        }

        public void Disable() {
            Reset();
            isActive = false;
        }

        void MainExplode() {
            meshRenderer.enabled = false;
            explosionRenderer.enabled = true;
            explosionAnimation.SetTrigger("Explode");
            isActive = false;
        }

        IEnumerator Explode() {
            meshRenderer.enabled = false;
            explosionRenderer.enabled = true;
            explosionAnimation.SetTrigger("Explode");
            yield return new WaitForSeconds(1f);
            isActive = false;
        }

        void Reset() {
            timer = 0f;
            radius = 0f;
            transform.localScale = Vector3.zero;

            isDangerous = false;
            isDeadByTimer = false;
            explosionRenderer.enabled = false;
            meshRenderer.enabled = true;
            meshRenderer.material.color = Color.white;
        }

        public bool IsActive() {
            return isActive;
        }
    }
}
